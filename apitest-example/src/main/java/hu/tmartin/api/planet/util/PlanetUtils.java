package hu.tmartin.api.planet.util;

import hu.tmartin.api.planet.dto.Planet;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Utils class for PlanetResponse.
 */
public abstract class PlanetUtils {

    /**
     * Gets a planet existance in planets array.
     * @param planets  @{@link Planet} array
     * @param planetName Search name
     * @return exists or not
     */
    public static boolean isPlanetInResponse(final Planet[] planets, final String planetName) {
        return !Arrays.asList(planets)
                .stream()
                .filter(p -> p.getName().equals(planetName))
                .collect(Collectors.toList()).isEmpty();
    }

}
