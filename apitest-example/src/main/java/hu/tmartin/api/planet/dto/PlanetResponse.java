package hu.tmartin.api.planet.dto;

/**
 * PlanetResponse POJO class
 * Generated file.
 */
public class PlanetResponse {

    private Planet[] results;

    private String previous;

    private int count;

    private String next;

    public Planet[] getPlanets() {
        return results;
    }

    public void setPlanets(Planet[] results) {
        this.results = results;
    }

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    @Override
    public String toString() {
        return "ClassPojo [planets = " + results + ", previous = " + previous + ", count = " + count + ", next = " + next + "]";
    }
}
