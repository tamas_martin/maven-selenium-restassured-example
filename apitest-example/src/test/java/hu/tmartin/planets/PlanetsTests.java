package hu.tmartin.planets;

import hu.tmartin.api.planet.dto.Planet;
import hu.tmartin.api.planet.dto.PlanetResponse;
import hu.tmartin.planets.dataproviders.PlanetTestDataProvider;
import hu.tmartin.planets.shared.AbstractApiTest;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import static hu.tmartin.api.planet.util.PlanetUtils.isPlanetInResponse;
import static io.restassured.RestAssured.get;

public class PlanetsTests extends AbstractApiTest {

    private static final int EXPECTED_PLANET_COUNT = 61;
    private static final int EXPECTED_PLANET_NUMBER_PER_RESPONSE = 10;

    /**
     * Testing http://swapi.co/api/planets/ API
     */
    @Test
    public void testAll() {

        Response response = get(API_ROOT);
        PlanetResponse planetResponse = response.as(PlanetResponse.class);
        Assert.assertEquals(planetResponse.getCount(), EXPECTED_PLANET_COUNT, "Planet count should be " + EXPECTED_PLANET_COUNT);

        Planet[] planets = planetResponse.getPlanets();
        Assert.assertEquals(planets.length, EXPECTED_PLANET_NUMBER_PER_RESPONSE,
                "Planet count per response should be " + EXPECTED_PLANET_NUMBER_PER_RESPONSE);
        Assert.assertTrue(isPlanetInResponse(planets, "Alderaan"), "Alderaan should be in the planet list");
    }

    /**
     * Testing http://swapi.co/api/planets/{id} API
     *
     * @param planetId Planet identifier
     * @param name     Name of the planet
     */
    @Test(dataProvider = "planet-provider", dataProviderClass = PlanetTestDataProvider.class)
    public static void getPlanetTest(int planetId, String name) {
        Response response = get(API_ROOT + planetId);
        Planet planet = response.as(Planet.class);
        Assert.assertTrue(planet.getName().equals(name));
    }
}
