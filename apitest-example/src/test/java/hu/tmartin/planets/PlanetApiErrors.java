package hu.tmartin.planets;

import hu.tmartin.planets.dataproviders.InvalidPlanetTestDataProvider;
import hu.tmartin.planets.shared.AbstractApiTest;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.when;

public class PlanetApiErrors extends AbstractApiTest {

    @Test(dataProvider = "invalid-planet-provider", dataProviderClass = InvalidPlanetTestDataProvider.class)
    public void testInvalidInput(Object planetId, int statusCode) {
        when().get(API_ROOT + planetId)
                .then().statusCode(statusCode);
    }

    @Test
    public void testPostRequestForGetPlanets() {
        when().post(API_ROOT)
                .then().statusCode(301);
    }

    @Test
    public void testPostRequestForGetPlanet() {
        when().post(API_ROOT + "1")
                .then().statusCode(301);
    }
}
