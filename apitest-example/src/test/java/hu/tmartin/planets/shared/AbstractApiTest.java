package hu.tmartin.planets.shared;

/**
 * Super class for API test classes.
 */
public abstract class AbstractApiTest {

    protected static final String API_ROOT = "http://swapi.co/api/planets/";
}
