package hu.tmartin.planets.dataproviders;

import org.testng.annotations.DataProvider;

public class InvalidPlanetTestDataProvider {

    @DataProvider(name = "invalid-planet-provider")
    public Object[][] dpMultiParam() {
        return new Object[][]{
                {-1, 404},
                {"53534534534543534534535", 404},
                {"$$$", 404},
                {"___", 404}};
    }
}
