package hu.tmartin.planets.dataproviders;

import org.testng.annotations.DataProvider;

public class PlanetTestDataProvider {

    @DataProvider(name = "planet-provider")
    public Object[][] dpMultiParam() {
        return new Object[][]{
                {1, "Tatooine"},
                {2, "Alderaan"},
                {3, "Yavin IV"},
                {4, "Hoth"}};
    }

}
