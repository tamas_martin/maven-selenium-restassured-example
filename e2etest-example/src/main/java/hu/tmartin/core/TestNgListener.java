package hu.tmartin.core;

import hu.tmartin.core.utils.ConfigUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

/**
 * Own test listener.
 */
public class TestNgListener implements ITestListener {

    private final Logger logger = LoggerFactory.getLogger(TestNgListener.class);

    @Override
    public void onTestStart(ITestResult iTestResult) {

    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {

    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {

    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {

    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

    }

    @Override
    public void onStart(ITestContext iTestContext) {
        logger.info("----------------------TEST STARTED, generating browser list----------------------");
        String threadCount = System.getProperty("uithreads");

        int threads = Integer.parseInt(threadCount);
        try {
            WebDriverProvider.setupBrowserDependencies();
            WebDriverProvider.generateBrowserPool(ConfigUtil.getBrowserName(), threads);
        } catch (Exception e) {
            logger.error("Browser is not supported", e);
        }
    }

    @Override
    public void onFinish(ITestContext iTestContext) {
        logger.info("----------------------TEST FINISHED----------------------");
        WebDriverProvider.clearWebDriverPool();
    }
}
