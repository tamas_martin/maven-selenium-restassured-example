package hu.tmartin.core;

/**
 * Enum for supported browsers.
 */
public enum BrowserType {
    CHROME, FIREFOX, IE, REMOTE, HEADLESS
}
