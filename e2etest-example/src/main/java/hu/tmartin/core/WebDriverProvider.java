package hu.tmartin.core;

import hu.tmartin.core.exceptions.BrowserNotSupportedException;
import hu.tmartin.core.utils.ConfigUtil;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Class to provide WebDriver object for tests.
 */
public class WebDriverProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebDriverProvider.class);

    private static List<WebDriverContainer> webDriverPool = new ArrayList<>();
    private static List<WebDriverContainer> safeList;
    private static Object lock = new Object();
    private static boolean browserDependenciesSet = false;
    private static boolean browserPoolIsSetUp = false;

    public static void clearWebDriverPool() {
        safeList.stream().unordered().forEach(p -> {
            p.getDriver().quit();
            LOGGER.debug("Closing a browser " + p.getId());
        });
        safeList = null;
    }

    public static WebDriverContainer getAvailableWebDriver(String browser) throws BrowserNotSupportedException {

        if(safeList == null) {
            throw new RuntimeException("safelist is empty");
        }
        WebDriverContainer availableHolder;
        while (true) {
            availableHolder = WebDriverContainer.getFreeHolder(safeList);
            if (availableHolder == null) {
                try {
                    LOGGER.debug("There is no available driver, lets wait a bit");
                    TimeUnit.MILLISECONDS.sleep(500);
                } catch (InterruptedException e) {
                    LOGGER.error("InterruptedException", e);
                }
            } else {
                break;
            }
        }
        availableHolder.setFree(false);
        return availableHolder;
    }

    public static void setupBrowserDependencies() throws BrowserNotSupportedException {
        if(browserDependenciesSet) {
            return;
        }
        final String browser = ConfigUtil.getBrowserName();
        switch (BrowserType.valueOf(browser.toUpperCase(Locale.getDefault()))) {
            case CHROME:
            case HEADLESS:
                WebDriverManager.chromedriver().setup();
                break;
            case FIREFOX:
                WebDriverManager.firefoxdriver().setup();
                break;
            case IE:
                WebDriverManager.iedriver().setup();
                break;
            case REMOTE:
                // Remote driver has to be set up by the user.
                break;
            default:
                throw new BrowserNotSupportedException(browser + " is not supported");
        }
        browserDependenciesSet = true;
        if(!browserPoolIsSetUp) {
            String uithreads = System.getProperty("uithreads");
            int threads = uithreads == null ?  Runtime.getRuntime().availableProcessors() : Integer.parseInt(uithreads);
            generateBrowserPool(browser, threads);
        }
    }

    public static synchronized void generateBrowserPool(String browser, int threads) {
        synchronized (lock) {

            if(browserPoolIsSetUp) {
                return;
            }
            LOGGER.debug("*************************** Generating browser stack" + threads);
            safeList = Collections.synchronizedList(webDriverPool);

            Runnable runnable = () -> {
                try {
                    LOGGER.debug("webDriverPool.size is " + safeList.size() + " creating new driver");
                    WebDriver driver = createNewDriver(browser);
                    WebDriverContainer holder = new WebDriverContainer(driver);
                    safeList.add(holder);
                } catch (BrowserNotSupportedException e) {
                    e.printStackTrace();
                }
            };
            List<Thread> threadList = new ArrayList<>();
            for (int i = 0; i< threads; i++) {
                Thread thread = new Thread(runnable);
                threadList.add(thread);
                thread.start();
            }
            for (Thread t: threadList) {
                try {
                    t.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            browserPoolIsSetUp = true;
            LOGGER.debug("Driver creation finished, webDriverPool.size is " + safeList.size());
        }
    }

    /**
     * Gets a WebDriver object.
     *
     * @param browser Browser name, see supported browsers.
     * @return WebDriver instance
     * @throws BrowserNotSupportedException
     */
    public static WebDriver createNewDriver(final String browser) throws BrowserNotSupportedException {

        WebDriver webDriver = null;
        switch (BrowserType.valueOf(browser.toUpperCase(Locale.getDefault()))) {
            case CHROME:
                webDriver = getChromeDriver();
                break;
            case HEADLESS:
                webDriver = getHeadLessDriver();
                break;
            case FIREFOX:
                webDriver = getFirefoxDriver();
                break;
            case IE:
                webDriver = getIEDriver();
                break;
            case REMOTE:
                try {
                    webDriver = new RemoteWebDriver(new URL(ConfigUtil.getHubUrl()), getChromeCapabilities());
                } catch (MalformedURLException e) {
                    LOGGER.error("Remote driver error", e);
                }
                break;
            default:
                throw new BrowserNotSupportedException(browser + " is not supported");
        }
        return webDriver;
    }

    private static WebDriver getIEDriver() {
        WebDriver webDriver;
        final InternetExplorerOptions ieoptions = new InternetExplorerOptions();
        ieoptions.introduceFlakinessByIgnoringSecurityDomains();
        ieoptions.ignoreZoomSettings();
        ieoptions.disableNativeEvents();
        webDriver = new InternetExplorerDriver(ieoptions);
        return webDriver;
    }

    private static WebDriver getFirefoxDriver() {
        WebDriver webDriver;
        final FirefoxOptions options = new FirefoxOptions();
        options.setCapability("marionette", true);
        webDriver = new FirefoxDriver(options);
        return webDriver;
    }

    private static WebDriver getChromeDriver() {
        WebDriver webDriver;
        final ChromeOptions chromeOptions = getChromeOptions();
        webDriver = new ChromeDriver(chromeOptions);
        return webDriver;
    }

    private static WebDriver getHeadLessDriver() {
        WebDriver webDriver;
        final ChromeOptions chromeOptions = getChromeOptions();
        chromeOptions.addArguments("headless");
        chromeOptions.addArguments("window-size=1200x600");
        chromeOptions.addArguments("--no-sandbox");
        chromeOptions.addArguments("--disable-gpu");
        webDriver = new ChromeDriver(chromeOptions);
        return webDriver;
    }

    private static ChromeOptions getChromeOptions() {
        final ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("disable-infobars");
        return chromeOptions;
    }

    private static DesiredCapabilities getChromeCapabilities() {
        final DesiredCapabilities chromeCap = DesiredCapabilities.chrome();
        final ChromeOptions options = getChromeOptions();
        chromeCap.setCapability(ChromeOptions.CAPABILITY, options);
        return chromeCap;
    }
}
