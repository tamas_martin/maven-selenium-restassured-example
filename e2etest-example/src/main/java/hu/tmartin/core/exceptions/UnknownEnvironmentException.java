package hu.tmartin.core.exceptions;

/**
 * Exception for handling not supported environments.
 */
public class UnknownEnvironmentException extends Exception {

    public UnknownEnvironmentException(String msg) {
        super(msg);
    }

}
