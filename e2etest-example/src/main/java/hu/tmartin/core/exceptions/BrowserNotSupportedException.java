package hu.tmartin.core.exceptions;

/**
 * Exception for handling not supported browsers.
 */
public class BrowserNotSupportedException extends Exception {

    public BrowserNotSupportedException(){
        super();
    }

    public BrowserNotSupportedException(String msg){
        super(msg);
    }
}
