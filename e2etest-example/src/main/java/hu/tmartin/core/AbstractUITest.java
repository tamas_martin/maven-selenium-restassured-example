package hu.tmartin.core;


import hu.tmartin.core.exceptions.BrowserNotSupportedException;
import hu.tmartin.core.utils.ConfigUtil;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

/**
 * Super class of UI tests.
 */
public abstract class AbstractUITest {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * {@link WebDriver} instance.
     */
    protected WebDriver driver;
    private WebDriverContainer container;

    public AbstractUITest() { }

    @BeforeSuite
    public void beforeSuite() throws BrowserNotSupportedException {
        WebDriverProvider.setupBrowserDependencies();
    }

    @BeforeClass
    public void setup() throws BrowserNotSupportedException {
        logger.debug(this.getClass().getSimpleName() + " requesting a free browser");
        WebDriverContainer driverHolder = WebDriverProvider.getAvailableWebDriver(ConfigUtil.getBrowserName());
        container = driverHolder;
        this.driver = driverHolder.getDriver();
        logger.debug(this.getClass().getSimpleName() + " has got the WebDriver: " + container.getId());
    }

    @AfterClass
    public void tearDown() {
        if (container != null) {
            logger.debug(this.getClass().getSimpleName() + " is releasing WebDriver: " + container.getId());
            driver.get("about:blank");
            driver.manage().deleteAllCookies();
            container.setFree(true);
        }
    }
}
