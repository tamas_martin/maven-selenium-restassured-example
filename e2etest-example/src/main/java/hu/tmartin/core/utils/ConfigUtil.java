package hu.tmartin.core.utils;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

/**
 * Util class to read configuration file.
 */
public final class ConfigUtil {

    private static String browser;
    private static String environment;
    private static String hubUrl;

    private ConfigUtil() {}

    /**
     * Gets the set browser name from configuration.
     * @return browser name value
     */
    public static synchronized String getBrowserName() {
        if (browser == null) {
            final Config config = ConfigFactory.load();
            browser = config.getString("browser");
        }
        return browser;
    }

    /**
     * Gets the environment name from configuration.
     * @return environment name value
     */
    public static synchronized String getEnvironment() {
        if (environment == null) {
            final Config config = ConfigFactory.load();
            environment = config.getString("environment");
        }
        return environment;
    }

    /**
     * Gets the selenium hub URL from configuration.
     * @return grid URL value
     */
    public static synchronized String getHubUrl() {
        if (hubUrl == null) {
            final Config config = ConfigFactory.load();
            hubUrl = config.getString("seleniumgridhub");
        }
        return hubUrl;
    }
}
