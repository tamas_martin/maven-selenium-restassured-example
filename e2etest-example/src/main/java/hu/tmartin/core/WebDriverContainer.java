package hu.tmartin.core;

import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.stream.Collectors;

public class WebDriverContainer {

    private static Logger LOGGER = LoggerFactory.getLogger(WebDriverContainer.class);
    private WebDriver driver;
    private boolean free;
    private static int counter = 0;
    private int id;

    public WebDriverContainer(WebDriver driver) {
        this.driver = driver;
        free = true;
        counter++;
        id = counter;
    }

    public int getId() {
        return id;
    }

    public boolean isFree() {
        return free;
    }

    public void setFree(boolean free) {
        this.free = free;
    }

    public WebDriver getDriver() {
        return driver;
    }

    /**
     * Gets the first free WebDriverContainer from the pool.
     * @param holders the pool
     * @return free WebDriverContainer instance
     */
    public static synchronized WebDriverContainer getFreeHolder(List<WebDriverContainer> holders) {
        List<WebDriverContainer> freeHolders = holders.stream().filter(p -> p.isFree()).collect(Collectors.toList());
        if(freeHolders.isEmpty()) {
            LOGGER.debug("There is no free WebDriverContainer, return null");
            return null;
        } else {
            WebDriverContainer freeOne = freeHolders.get(0);
            LOGGER.debug("Getting a free driver, ID: " + freeOne.id);
            return freeOne;
        }
    }
}
