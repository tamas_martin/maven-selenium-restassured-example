package hu.tmartin.pages;

import hu.tmartin.pages.common.AbstractPage;
import org.openqa.selenium.WebDriver;

/**
 * Page object for the Solo movie page.
 */
public class SoloMoviePage extends AbstractPage {

    private String pagePath = "/films/solo";
    private String pageTitle = "Solo: A Star Wars Story | StarWars.com";

    public SoloMoviePage(WebDriver driver) {
        super(driver);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getPagePath() {
        return pagePath;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getPageTitle() {
        return pageTitle;
    }
}
