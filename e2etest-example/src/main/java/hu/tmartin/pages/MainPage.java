package hu.tmartin.pages;

import hu.tmartin.pages.common.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

/**
 * Page object for the Main page.
 */
public class MainPage extends AbstractPage {

    private String path = "/";
    private String title = "StarWars.com | The Official Star Wars Website";

    @Override
    public String getPagePath() {
        return path;
    }

    @Override
    public String getPageTitle() {
        return title;
    }

    public MainPage(WebDriver driver) {
        super(driver);
    }

    public WebElement videosLink() {
        return driver.findElement(By.cssSelector("a[data-section='video']"));
    }

    public WebElement filmsLink() {
        return driver.findElement(By.cssSelector("a[data-section='films']"));
    }

    public VideosPage openVideoPage() {
        WebElement videosLink = videosLink();
        Assert.assertTrue(videosLink.isDisplayed());
        videosLink.click();
        VideosPage videosPage = new VideosPage(driver);
        videosPage.waitUntilPageIsLoaded();
        return videosPage;
    }

    public FilmsPage openFilmsPage() {
        WebElement filmsLink = filmsLink();
        Assert.assertTrue(filmsLink.isDisplayed());
        filmsLink.click();
        FilmsPage filmsPage = new FilmsPage(driver);
        filmsPage.waitUntilPageIsLoaded();
        return filmsPage;
    }
}
