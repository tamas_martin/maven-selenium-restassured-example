package hu.tmartin.pages;

import hu.tmartin.pages.common.AbstractPage;
import org.openqa.selenium.WebDriver;

/**
 * Page object for the Videos page.
 */
public class VideosPage extends AbstractPage {

    private String path = "/video";
    private String title = "Star Wars Videos and Behind the Scenes Featurettes | StarWars.com";

    @Override
    public String getPagePath() {
        return path;
    }

    @Override
    public String getPageTitle() {
        return title;
    }

    public VideosPage(WebDriver driver) {
        super(driver);
    }

}
