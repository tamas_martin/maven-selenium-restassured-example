package hu.tmartin.pages.common;

import hu.tmartin.core.utils.ConfigUtil;
import hu.tmartin.core.exceptions.UnknownEnvironmentException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

/**
 * Super class of page objects.
 */
public abstract class AbstractPage {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    protected String host;

    protected WebDriver driver;

    /**
     * Gets the page path after the domain name.
     *
     * @return
     */
    public abstract String getPagePath();

    /**
     * Gets the page title.
     *
     * @return
     */
    public abstract String getPageTitle();

    /**
     * Constructor for the Page.
     * @param driver {@link WebDriver} instance
     */
    public AbstractPage(WebDriver driver) {
        this.driver = driver;
        try {
            setupHost();
        } catch (UnknownEnvironmentException e) {
            logger.error("Environment does not exist", e);
            Assert.fail("There is no configured environment.");
        }
    }

    /**
     * Opens the page and waits for it is loaded completely.
     */
    public void open() {
        logger.info("Opening the page " + host + getPagePath());
        driver.get(host + getPagePath());
        waitUntilPageIsLoaded();
    }

    /**
     * Waits while page is visible. Checks Page path.
     */
    public void waitUntilPageIsLoaded() {
        waitForDocumentReady();
        Assert.assertTrue(driver.getCurrentUrl().contains(
                getPagePath()));
    }

    /**
     * Waits while page is visible. Checks Page title.
     */
    public void waitForVisible() {
        waitForDocumentReady();
        Assert.assertTrue(driver.getTitle().equals(getPageTitle()));
    }

    private void setupHost() throws UnknownEnvironmentException {
        final String environment = ConfigUtil.getEnvironment();
        switch (environment) {
            case "prod":
                host = "https://www.starwars.com";
                break;
            default:
                throw new UnknownEnvironmentException(environment + " is not supported.");
        }
    }

    private void waitForDocumentReady() {
        new WebDriverWait(driver, 10).until(
                webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
    }

}
