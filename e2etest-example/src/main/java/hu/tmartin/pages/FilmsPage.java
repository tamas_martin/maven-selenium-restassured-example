package hu.tmartin.pages;

import hu.tmartin.pages.common.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

/**
 * Page object for the Films page.
 */
public class FilmsPage extends AbstractPage {

    private String path = "/films";
    private String title = "Star Wars Movies | StarWars.com";

    public FilmsPage(WebDriver driver) {
        super(driver);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getPagePath() {
        return path;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getPageTitle() {
        return title;
    }

    public WebElement getSoloMovieImg() {
        return driver.findElement(By.cssSelector("img[alt='Solo: A Star Wars Story']"));
    }

    /**
     * Opens the Solo movie page on Films page.
     * @return {@link SoloMoviePage} instance
     */
    public SoloMoviePage openSoloMoviePage() {
        WebElement soloMovieImg = getSoloMovieImg();
        Assert.assertTrue(soloMovieImg.isDisplayed());
        soloMovieImg.click();
        return new SoloMoviePage(driver);
    }
}
