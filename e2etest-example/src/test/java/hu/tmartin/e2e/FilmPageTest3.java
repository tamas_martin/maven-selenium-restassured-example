package hu.tmartin.e2e;

import hu.tmartin.core.AbstractUITest;
import hu.tmartin.pages.FilmsPage;
import hu.tmartin.pages.MainPage;
import hu.tmartin.pages.SoloMoviePage;
import org.testng.annotations.Test;

public class FilmPageTest3 extends AbstractUITest {

    @Test
    public void filmPageTest() {
        logger.info("Testing the Films Page");
        MainPage mainPage = new MainPage(driver);
        mainPage.open();
        mainPage.waitUntilPageIsLoaded();

        FilmsPage films = mainPage.openFilmsPage();
        films.waitUntilPageIsLoaded();

        SoloMoviePage soloPage = films.openSoloMoviePage();
        soloPage.waitForVisible();
    }
}
