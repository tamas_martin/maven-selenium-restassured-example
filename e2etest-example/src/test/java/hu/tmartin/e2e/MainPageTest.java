package hu.tmartin.e2e;

import hu.tmartin.core.AbstractUITest;
import hu.tmartin.pages.FilmsPage;
import hu.tmartin.pages.MainPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class MainPageTest extends AbstractUITest {

    @Test
    public void mainPageTest() {
        logger.info("Testing the Main Page");
        MainPage mainPage = new MainPage(driver);
        mainPage.open();
        mainPage.waitUntilPageIsLoaded();
        Assert.assertTrue(driver.getTitle().equals(mainPage.getPageTitle()));
        FilmsPage films = mainPage.openFilmsPage();
        films.waitForVisible();
    }
}
