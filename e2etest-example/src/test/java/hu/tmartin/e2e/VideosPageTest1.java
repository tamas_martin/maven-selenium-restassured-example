package hu.tmartin.e2e;

import hu.tmartin.core.AbstractUITest;
import hu.tmartin.pages.MainPage;
import hu.tmartin.pages.VideosPage;
import org.testng.annotations.Test;

public class VideosPageTest1 extends AbstractUITest {

    @Test
    public void testVideosPage() {
        MainPage mainPage = new MainPage(driver);
        mainPage.open();
        VideosPage videospage = mainPage.openVideoPage();
        videospage.waitUntilPageIsLoaded();
    }
}
