# Maven-Java-Selenium-Restassured-Example

##What is this repository for?
Providing an example of using maven, java, selenium, restassured in test automation

##How to set up?
You need installed maven

You need JDK 8

##Run all tests
>mvn test

>mvn test -Dbrowser=firefox

Supported browsers are **firefox, chrome, ie, remote, headless** (You don't need to download and set up chromedriver or geckodriver, it is done automatically)

For running the test remotely, you need to set _seleniumgridhub_ property in application.properties file.

## Running the API or the UI project separately

>mvn test -pl apitest-example

>mvn test -pl e2etest-example -Duithreads=6
